/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package ox;

/**
 *
 * @author ASUS
 */
public class OX {

    private String[][] table = { // ตาราง
        {"-", "-", "-"},
        {"-", "-", "-"},
        {"-", "-", "-"}
    };
    private int turn = 0;
    private int count = 0;
    public void showtable() { // ลูปแสดงตาราง
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public void input() { 
        checkturn();
        System.out.println("Please input row, col:");
    }
    
    public boolean checkbug(int row, int col) { // เช็คบัค กรณี X หรือ O ซ้ำตำแหน่ง & เช็คบัค เกินตาราง
        if(row <= 3 && row >= 1 && col <= 3 && col >= 1 && table[row - 1][col - 1].equals("-") ){
            return true;
        }else{
            System.out.println("\nError !!!! \n");
            return false;
        }
    }

    private void checkturn() { // เช็ค turn 
        if (turn == 0) {
            System.out.println("Turn O");
        } else {
            System.out.println("Turn X");
        }
    }

    public int showXO(int row, int col) { // โชว์ X หรือ O ในตาราง
        if (turn == 0) {
            table[row - 1][col - 1] = "O";
        } else {
            table[row - 1][col - 1] = "X";
        }
        System.out.println("");
        return 0;
    }
   

    public void turn() { // สลับ turn
        if (turn == 0) {
            turn = turn + 1;
        } else {
            turn = turn - 1;
        }
    }

    public int checkwin() { // วนลูป เช็ค Win
        for (int i = 0; i < 3; i++) {
            if (table[0][i].equals(table[1][i]) && table[0][i].equals(table[2][i]) && !table[0][i].equals("-")) {  // เช็คกรณี | แถว
                if (table[0][i].equals("O")) {
                    return 1;
                } else {
                    return 2;
                }
            } else if (table[i][0].equals(table[i][1]) && table[i][0].equals(table[i][2]) && !table[i][0].equals("-")) { // เช็คกรณี -- คอลัม
                if (table[i][0].equals("O")) {
                    return 1;
                } else {
                    return 2;
                }
            }
        }
        if (table[0][0].equals(table[1][1]) && table[0][0].equals(table[2][2]) && !table[0][0].equals("-")) { // เช็คกรณีแนวเฉียง \ ซ้าย บน ไป ขวาล่าง
                if (table[0][0].equals("O")) {
                    return 1;
                } else {
                    return 2;
                }
            } else if (table[0][2].equals(table[1][1]) && table[0][2].equals(table[2][0]) && !table[0][2].equals("-")) { // เช็คกรณีแนวเฉียง / ขวา บน ไป ซ้ายล่าง
                if (table[0][2].equals("O")) {
                    return 1;
                } else {
                    return 2;
                }
            }
        count++;
        if(count == 9){
            return 3;
        }
        return 0;
    }
    
    public void showwinX() { // แสดง X Win
        System.out.println("");
        System.out.println(">>>X Win<<<");
    }
    public void showwinO() { // แสดง O Win
        System.out.println("");
        System.out.println(">>>O Win<<<");
    }
    public void showDraw() { // แสดง Draw
        System.out.println("");
            System.out.println(">>>Draw<<<");
    }

}
